use master
go

--------------------------
if exists(select * from sysdatabases where name = 'PruebaTecnica')
begin
	drop database PruebaTecnica
end
go


create database PruebaTecnica
go


use PruebaTecnica
go

create table TipoCuenta
(
	TipoCuentaId varchar (50) not null primary key,
	Nombre varchar(50) not null,
)
go

create table Cuentas
(
	CuentaId int not null primary key,
	TipoCuentaId varchar(50) not null foreign key references TipoCuenta(TipoCuentaId),
	Saldo int not null,
	UsuarioId varchar (50) 
)
go

create table Transferencias
(
	TransferenciaId int IDENTITY(1,1) not null primary key,
	CuentaIdOrigen int not null foreign key references Cuentas(CuentaId),
	CuentaIdDestino int not null foreign key references Cuentas(CuentaId),
	Monto int not null,
	Descripcion varchar (50),
	Fecha date not null
)
go

create table Usuarios
(
	CuentaId int not null foreign key references Cuentas(CuentaId),
	Contraseņa varchar (50) not null
)
go

insert TipoCuenta(Nombre,TipoCuentaId) values ('Damian','credito')
insert Cuentas (CuentaId,Saldo,TipoCuentaId,UsuarioId) values (1995,30000,'credito','d-bozz')
insert Usuarios (CuentaId,Contraseņa) values (1995,'qwerty123')


insert TipoCuenta(Nombre,TipoCuentaId) values ('Damian','debito')
insert Cuentas (CuentaId,Saldo,TipoCuentaId,UsuarioId) values (2017,10000,'debito','tefi')
insert Usuarios (CuentaId,Contraseņa) values (2017,'qwerty123')




go
Create Procedure SP_Logueo
--Alter Procedure SP_Logueo 
 @CuentaId int,
 @Contraseņa varchar(50) 
  AS
Begin
	SELECT C.CuentaId, u.Contraseņa
	From Usuarios u inner join Cuentas C on u.CuentaId = C.CuentaId
	Where C.CuentaId = @CuentaId and u.Contraseņa = @Contraseņa
End
go


CREATE PROCEDURE SP_Transferir
--ALTER PROCEDURE SP_Transferir
@CuentaId int,
@Contraseņa varchar(50), 
@CuentaIdOrigen INT,
@CuentaIdDestino INT,
@Monto INT,
@Descripcion VARCHAR(50)
AS

if EXISTS (SELECT C.CuentaId, u.Contraseņa
	From Usuarios u inner join Cuentas C on u.CuentaId = C.CuentaId
	Where C.CuentaId = @CuentaId and u.Contraseņa = @Contraseņa )
	BEGIN
IF EXISTS (SELECT CuentaId FROM Cuentas WHERE CuentaId = @CuentaIdOrigen AND saldo >= @monto)
BEGIN
	IF EXISTS(SELECT CuentaId FROM Cuentas WHERE CuentaId = @CuentaIdDestino) AND 
		(@monto IS NOT NULL)
		BEGIN
			BEGIN TRANSACTION
			UPDATE Cuentas SET saldo=saldo-@monto WHERE CuentaId = @CuentaIdOrigen
			IF @@ERROR<>0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN 1
				END    
			UPDATE Cuentas SET saldo = saldo+@monto WHERE CuentaId = @CuentaIdDestino
			IF @@ERROR<>0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN 1
				END
			INSERT INTO Transferencias VALUES (@CuentaIdOrigen, @CuentaIdDestino, @monto, @Descripcion, getDate())
			IF @@ERROR<>0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN 2
				END
			COMMIT TRANSACTION
			RETURN 0
		END
	ELSE
		RETURN 3
	END
RETURN 4
END 
RETURN 5	
 GO




 CREATE PROC ListarUsuarios
AS
BEGIN
SELECT * FROM Usuarios
END
GO
 
 CREATE PROC ListarTransferencias
AS
BEGIN
SELECT * FROM Transferencias
END
GO


CREATE PROC BuscarUsuario
--ALTER PROC BuscarUsuario

@CuentaId int,
@Contraseņa varchar(50)
as
BEGIN
SELECT * FROM Usuarios WHERE CuentaId = @CuentaId and Contraseņa = @Contraseņa
END
GO


select * from Cuentas
select * from Transferencias
select * from usuarios