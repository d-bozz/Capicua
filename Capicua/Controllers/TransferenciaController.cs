﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using Capicua.Models;
using System.Web.Http.Cors;
namespace Capicua.Controllers
{
    /* Enable to same origin */
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]

    public class TransferenciaController : ApiController
    {

       

        // POST: api/Transferencia
        public Transferencias Post(Transferencias value)
        {

                var contra = value.Contraseña;
                var origen = value.CuentaIdOrigen; 
                var destino = value.TransferenciaId;
                var monto = value.Monto;
                var descripcion = value.Descripcion;

                if (string.IsNullOrEmpty(contra))
                    throw new Exception("La contraseña no puede ser vacia.");

                if (origen <= 0)
                    throw new Exception("El numero de la cuenta de origen debe ser positivo");

                if (destino <= 0)
                    throw new Exception("El numero de la cuenta de destino debe ser positivo");

                if (monto <= 0)
                    throw new Exception("El monto debe ser positivo");

                if (string.IsNullOrEmpty(descripcion))
                    throw new Exception("La descricion no puede ser vacia.");

                Transferencias Transferencias = new Transferencias(contra, 1, origen, destino, DateTime.Today, monto, descripcion);
                Transferencias.RealizarTrans(Transferencias);

                return Transferencias;
        }

    }
}
