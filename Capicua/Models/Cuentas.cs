﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Capicua.Models
{
    public class Cuentas
    {
        //Atributos
        private int _CuentaId;
        private string _UsuarioId;
        private string _TipoCuentaId;
        private int _Saldo;


        //propiedades
        public int CuentaId
        {
            get { return _CuentaId; }
            set { _CuentaId = value; }
        }

        public string TipoCuentaId
        {
            get { return _TipoCuentaId; }
            set { _TipoCuentaId = value; }
        }

        public int Saldo
        {
            get { return _Saldo; }
            set { _Saldo = value; }
        }

        public string UsuarioId
        {
            get { return _UsuarioId; }
            set { _UsuarioId = value; }
        }

        //constructor
        public Cuentas (int pCuentaId, string pUsuarioId, string pTipoCuentaId, int pSaldo)
        {
            CuentaId = pCuentaId;
            UsuarioId = pUsuarioId;
            TipoCuentaId = pTipoCuentaId;
            Saldo = pSaldo;

        }
    }
}