﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Capicua.Models
{
    public class TipoCuentas
    {
        //Atributos
        private string _TipoCuentaId;
        private string _Nombre;

        //Propiedades
        public string TipoCuentaId
        {
            get { return _TipoCuentaId; }
            set { _TipoCuentaId = value; }
        }

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        //Constructor
        public TipoCuentas(string pTipoCuentaId, string pNombre)
        {
            TipoCuentaId = pTipoCuentaId;
            Nombre = pNombre;

        }
    }
}