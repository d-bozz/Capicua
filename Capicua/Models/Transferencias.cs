﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Capicua.Models
{
    public class Transferencias
    {
        //atributos
        private string _Contraseña;
        private int _TransferenciaId;
        private int _CuentaIdOrigen;
        private DateTime _Date;
        private int _CuentaIdDestino;
        private int _Monto;
        private string _Descripcion;


        //propiedades
        public string Contraseña
        {
            get { return _Contraseña; }
            set { _Contraseña = value; }
        }


        public int TransferenciaId
        {
            get { return _TransferenciaId; }
            set { _TransferenciaId = value; }
        }

        public int CuentaIdOrigen
        {
            get { return _CuentaIdOrigen; }
            set { _CuentaIdOrigen = value; }
        }

        public int CuentaIdDestino
        {
            get { return _CuentaIdDestino; }
            set { _CuentaIdDestino = value; }
        }

        public int Monto
        {
            get { return _Monto; }
            set { _Monto = value; }
        }

        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
        

        //constructor
        public Transferencias(string p_Contraseña, int p_TransferenciaId, int p_CuentaIdOrigen, int p_CuentaIdDestino, DateTime p_Date, int p_Monto, string p_Descripcion)
        {
            Contraseña = p_Contraseña;
            TransferenciaId = p_TransferenciaId;
            CuentaIdOrigen = p_CuentaIdOrigen;
            CuentaIdDestino = p_CuentaIdDestino;
            Date = p_Date;
            Monto = p_Monto;
            Descripcion = p_Descripcion;


        }

        //operaciones
       
        public static List<Transferencias> ListarTransferencias()
        {

            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                List<Transferencias> ListarTransferencias = new List<Transferencias>();
                Transferencias trans = null;
                SqlCommand cmd = new SqlCommand("ListarTransferencias", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    trans = new Transferencias((string)lector[0], 1, Convert.ToInt32(lector[2]),
                        Convert.ToInt32(lector[3]), DateTime.Today, Convert.ToInt32(lector[5]), (string)lector[6]);
                    ListarTransferencias.Add(trans);
                }

                lector.Close();
                return ListarTransferencias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { cnn.Close(); }
        }
       
        public static void RealizarTrans(Transferencias trans)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("SP_Transferir", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Contraseña", trans.Contraseña);
                cmd.Parameters.AddWithValue("@CuentaIdOrigen", trans.CuentaIdOrigen);
                cmd.Parameters.AddWithValue("@CuentaIdDestino", trans.CuentaIdDestino);
                cmd.Parameters.AddWithValue("@Monto", trans.Monto);
                cmd.Parameters.AddWithValue("@Descripcion", trans.Descripcion);
                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);

                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == 1)
                    throw new Exception("No se pudieron modificar las cuentas");
                if (valorRetorno == 2)
                    throw new Exception("No se pudo crear la transferencia");
                if (valorRetorno == 3)
                    throw new Exception("Alguna de las cuentas no es válida o no se proporcionó un monto");
                if (valorRetorno == 4)
                    throw new Exception("Saldo Insuficiente");
                if (valorRetorno == 5)
                    throw new Exception("Cuenta de origen o contraseña incorrectas");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { cnn.Close(); }
        }



    }
}