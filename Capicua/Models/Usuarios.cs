﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Capicua.Models
{
    public class Usuarios
    {
        //Atributos
        private int _CuentaId;
        private string _Contraseña;


        //Propiedades
        public int CuentaId
        {
            get { return _CuentaId; }
            set { _CuentaId = value; }
        }


        public string Contraseña
        {
            get { return _Contraseña; }
            set { _Contraseña = value; }
        }

        //Constructor
        public Usuarios(int pCuentaId, string pContraseña)
        {
            CuentaId = pCuentaId;
            Contraseña = pContraseña;

        }

    }
}